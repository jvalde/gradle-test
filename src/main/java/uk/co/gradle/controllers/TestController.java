package uk.co.gradle.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import uk.co.gradle.services.TestService;


@Controller
public class TestController {

    @RequestMapping("home")
    public String loadHomePage(Model m) {
        m.addAttribute("name", new TestService().getName());
        return "home";
    }
}
